[LINK](https://medium.com/@med.phisiker/%D0%B8%D1%81%D0%BF%D0%BE%D0%BB%D1%8C%D0%B7%D1%83%D0%B5%D0%BC-gpu-nvidia-%D0%B2%D0%BD%D1%83%D1%82%D1%80%D0%B8-docker-%D0%BA%D0%BE%D0%BD%D1%82%D0%B5%D0%B9%D0%BD%D0%B5%D1%80%D0%B0-e6aa73c40442)
Данная статья была подготовлена для доклада `Используем GPU nvidia внутри docker-контейнера` ([ссылка на видеозапись](https://youtu.be/NyAXipOWz48)) для участников онлайн курса `MLOps и production подход к ML исследованиям 2.0` ([ссылка на курс](https://ods.ai/tracks/ml-in-production-spring-23)).

Код используемый в данной статье размещен в `github`-репозитории ([ссылка](https://github.com/medphisiker/docker_yolov8)). Файл презентации доклада так же размещен в данном репозитории 
`./Презентация.pdf`.

> О Docker

`Docker` — программа, позволяющая операционной системе(ОС) запускать процессы в изолированном окружении на базе специально созданных образов(`Docker-образов`).

![](https://miro.medium.com/v2/resize:fit:1050/1*ovRuAuqPf4r2xpiWh71rUg.png)

> Преимущества использования Docker-контейнеров

`Docker-контейнеры` упрощают работу ит-специалистов, которые разрабатывают и развертывают приложения:
	
- `Docker` устраняет проблемы зависимостей и рабочего окружения

`Docker` позволяет упаковать в `Docker-образ` приложение и все его зависимости: пакеты, системные утилиты и библиотеки для языков программирования. Контейнеры содержат в себе все необходимое для запуска приложения.

- Изоляция благодаря виртуализации

Контейнер — это набор процессов, изолированных от основной ОС. Приложения работают внутри контейнера и не имеют доступа к нашей ОС. Это повышает безопасность, — приложения, запущенные в контейнере, не могут навредить нашей ОС.

- Ускорение развертывания приложений

Контейнеры позволяют существенно автоматизировать процесс, развертывания, так как включают в себя все нужные зависимости и порядок выполнения действий для запуска приложения.

- Масштабирование

Мы можем запускать контейнеры одного вида сразу на нескольких серверах и масштабировать нагрузку от пользователей.

- Микросервисная архитектура

Микросервисная архитектура, - это подход к разработке, при котором приложение разбивается на небольшие независимые компоненты, взаимодействующие двуг с другом.

Такая модульность позволяет быстрее разрабатывать и тестировать новые функции. Важно сохранять формат обмена сообщениями между отдельными компонентами, как устроен внутри сам компонент уже не так важно.

**Основные компоненты Docker**

- `Docker Client` - утилита для терминала для общения пользователя с `Docker Daemon`.
- `Docker Daemon` - служба, которая отвечает за создание, запуск и удаление контейнеров.
- `Dockerfile`**-** файл с последовательностью расположенных в нем инструкций для создания `Docker-образа`.
- `Image` **-** неизменяемый файл (образ), из которого можно развернуть контейнер.
- `Container` - изолированное окружение, которое развернули из образа.
- `Docker Hub` **-** популярный реестр `Docker-образов` используемый по умолчанию в Docker.

![](https://miro.medium.com/v2/resize:fit:1050/1*aIlLOzHXg_Y__BimTTFcqg.png)
![[Pasted image 20230930214354.png]]
Источник: [https://t1.daumcdn.net/cfile/tistory/99D0DE3A5C9AA74119](https://t1.daumcdn.net/cfile/tistory/99D0DE3A5C9AA74119)

О nvidia-docker

Для того, чтобы `Docker-контейнеры` могли использовать в своих расчетах видеокарту(GPU) нужно установить `nvidia-docker`.

`nvidia-docker` обеспечивает контейнерную виртуализацию аппаратного обеспечения GPU, подобно тому как сам `Docker` делает это для процессора CPU.

![](https://miro.medium.com/v2/resize:fit:783/1*THInW6v1p-K_X1YqlQkGwg.png)

Подробнее об архитектуре `nvidia-docker` можно почитать в официальной документации ([ссылка](https://docs.nvidia.com/datacenter/cloud-native/container-toolkit/arch-overview.html#arch-overview)).

Важно отметить, что доступ к виртуализованной GPU осуществляется по средствам дайвера GPU в нашей ОС. Драйвер `nVidia GPU` внутри `Docker-контейнера` и нашей `host ОС` будут одинаковыми.

Но все, что устанавливается поверх драйвера может отличаться, например, версия `nVidia CUDA`.

# Установка `nvidia-docker` на Linux (платформа ARM64)

В целом для установки `nvidia-docker` на ОС `Linux` нужно выполнить следующие шаги:

1. Установить драйвер для nVidia GPU
2. Установить в нашу ОС `Docker` и настроить его стандартным образом
3. Установить `nvidia-docker` поверх `Docker`

## Шаг 1 Установить драйвер для nVidia GPU

[[Nvidia]] - Лучший способ

Данный шаг знаком многим и по нему много хороших руководств например ([ссылка](https://help.ubuntu.ru/wiki/%D0%B4%D1%80%D0%B0%D0%B9%D0%B2%D0%B5%D1%80_%D0%B2%D0%B8%D0%B4%D0%B5%D0%BE%D0%BA%D0%B0%D1%80%D1%82_nvidia) на русскоязычную документацию по Ubuntu).

Отмечу, что в официальной документации по установке `nvidia-docker` ([ссылка](https://docs.nvidia.com/datacenter/cloud-native/container-toolkit/install-guide.html#nvidia-drivers)) рекомендуют способ получения драйверов для nVidia GPU из официальных репозиториев используемого вами дистрибутива Linux.

![](https://miro.medium.com/v2/resize:fit:1050/1*F7AjBI4bspKGPqUBHk_21w.png)

Но способ при котором мы скачиваем файл драйвера с официального сайта Nvidia по [ссылке](https://www.nvidia.com/Download/index.aspx?lang=en-us) (допустим мы скачали файл `NVIDIA-Linux-x86_64-510.47.03.run`), и затем выполняем его сборку из исходников выполняя команду:

sudo sh NVIDIA-Linux-x86_64-510.47.03.run

с последующей перезагрузкой ПК тоже можно использовать и я использовал именно его.

## Особенности Docker из snap

Часто мы начинаем с шага (2), когда у нас уже есть ОС Linux с установленным на ней драйвером для nVidia GPU, либо шага (3), когда в нашей ОС уже присутствует Docker.

Я производил установку на `Linux Ubuntu Server 20.04`. У данного дистрибутива есть особенность, — при установке ОС он предлагает нам установить `Docker`.

![](https://miro.medium.com/v2/resize:fit:953/1*43nAxLJaSaaNoSNuY8-9-w.png)

Это довольно удобно и многие пользователи соглашаются.

Кажется `Docker` уже установлен в нашей ОС, можно начинать с шага (3) и добавить `nvidia-docker` согласно инструкциям в руководстве от `nVidia` [(ссылка](https://docs.nvidia.com/datacenter/cloud-native/container-toolkit/install-guide.html)).

Ньанс заключается в том, что если мы соглашались установить `Docker` при установке `Linux Ubuntu Server 20.04` , то он будет установлен через менеджер приложений `snap`.

Как обычный `Docker` он будет работать нормально, но `nvidia-docker` установить на него будет нельзя. Дело в том, что `snap` и все его приложения тоже имеют некоторый уровень виртуализации.

Из-за этого у нас возникнет ошибка `"docker: Error response from daemon: could not select device driver "" with capabilities: [[gpu]]."` ([ссылка на проблему и ее решение](https://github.com/NVIDIA/nvidia-docker/issues/1243#issuecomment-669545062)). Данная ошибка связана с тем, что `nvidia-docker` устанавливаемый поверх `Docker` из `snap` из за виртуализации не имеет доступа к nVidia GPU и ему кажется, что ее нет в нашей системе.

Решение состоит в том, чтобы удалить `Docker` из `snap`, и установить обычный `Docker` непосредственно в нашу ОС.

Проверим установлен ли в нашей ОС пакетный менеджер Snap и какие программу установлены внутри него, выполнив команду:

snap list

Пример вывода из терминала:


```shell
Name               Version                     Rev    Tracking       Publisher   Notes  
bare               1.0                         5      latest/stable  canonical✓  base  
chromium-ffmpeg    0.1                         30     latest/stable  canonical✓  -  
core18             20230320                    2721   latest/stable  canonical✓  base  
core20             20230404                    1879   latest/stable  canonical✓  base  
core22             20230404                    617    latest/stable  canonical✓  base  
epiphany           42.4                        143    latest/stable  jbicha      -  
gnome-3-28-1804    3.28.0-19-g98f9e67.98f9e67  198    latest/stable  canonical✓  -  
gnome-3-38-2004    0+git.6f39565               140    latest/stable  canonical✓  -  
gnome-42-2204      0+git.587e965               99     latest/stable  canonical✓  -  
gtk-common-themes  0.1-81-g442e511             1535   latest/stable  canonical✓  -  
lxd                4.0.9-a29c6f1               24061  4.0/stable/…   canonical✓  -  
snapd              2.59.2                      19122  latest/stable  canonical✓  snapd
```


Если среди них есть `Docker` , его необходимо удалить из `snap` , выполнив команду:

sudo snap remove --purge docker

Теперь нам нужно вернуться на шаг 2 установить `Docker` непосредственно в ОС `Linux Ubuntu Server 20.04`, настроить его сервисы(демоны) и затем перейти к шагу 3 установить `nvidia-docker` по инструкциям от nvidia [(ссылка](https://docs.nvidia.com/datacenter/cloud-native/container-toolkit/install-guide.html)).

## Шаг 2 Установим в нашу ОС `Docker` и настроим его стандартным образом

Установим `Docker` в нашу ОС `Ubuntu Server 20.04` согласно официальной документации([ссылка](https://docs.docker.com/engine/install/ubuntu/)). Там же будут приведены инструкции и для других дистрибутивов Linux.

Для `Ubuntu Server 20.04` выполним в терминале команды:

## Установим Docker  

```shell
sudo apt update  
sudo apt install \  
    apt-transport-https \  
    ca-certificates \  
    curl \  
    gnupg-agent \  
    software-properties-common 
```
 
  
## Добавим Docker GPG key  

```
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -  
```

  
## Добавляем необходимые репозитории  

```
sudo add-apt-repository \  
   "deb [arch=amd64] https://download.docker.com/linux/ubuntu \  
   $(lsb_release -cs) \  
   stable"  
```

  
## Обновляем список пакетов для apt и устанавливаем компоненты Docker  

`sudo apt update && sudo apt install docker-ce docker-ce-cli containerd.io

После этого проверим корректность установки `Docker` и его компонентов выполнив запуск нашего самого первого `Docker-контейнера`, приветствующего нас в мире контейнерной виртуализации.

По аналогии с самой первой программой которую обычно пишут программисты при изучение для себя нового языка программирования данный `Docker-образ` называется `hello-world`.

Выполним команду в терминале:


```
sudo docker run hello-world
```


Если `Docker` не обнаружит данный образ локально в нашей ОС, то он скачает(выполнит `Pulling image`) его из `Docker Hub`. В терминале мы увидим вывод:

```
Unable to find image 'hello-world:latest' locally  
latest: Pulling from library/hello-world  
2db29710123e: Pull complete   
Digest: sha256:e74fcc1c1b5a1f05feda3eec4d6f0dbe306fd7da45d4769ea6063d52746cf41b  
Status: Downloaded newer image for hello-world:latest
```

После скачивания `Docker-образа` на его основе будет запущен `Docker-контейнер` `hello-world` и в терминале появится сообщение:

```
Hello from Docker!  
This message shows that your installation appears to be working correctly.  
  
To generate this message, Docker took the following steps:  
 1. The Docker client contacted the Docker daemon.  
 2. The Docker daemon pulled the "hello-world" image from the Docker Hub.  
 (amd64)  
 3. The Docker daemon created a new container from that image which runs the  
 executable that produces the output you are currently reading.  
 4. The Docker daemon streamed that output to the Docker client, which sent it  
 to your terminal.  
To try something more ambitious, you can run an Ubuntu container with:  
 $ docker run -it ubuntu bash  
Share images, automate workflows, and more with a free Docker ID:  
 https://hub.docker.com/  
For more examples and ideas, visit:  
 https://docs.docker.com/get-started/
```

В котором нам сообщат, что установка `Docker` и его компонентов прошла успешно. Так же нам расскажут как происходил запуск данного `Docker-контейнера`:

1. `Docker client` (утилита которую мы вызываем набирая в терминале команду Docker) сообщает службе `Docker daemon` о запуске контейнера “hello-world”.
2. Не найдя `Docker-образ` `hello-world` в нашей системе, `Docker daemon` скачает его (выполнит `Pulling image`) из `Docker Hub` .
3. Служба `Docker daemon` запустит новый `Docker-контейнер` из скаченного `Docker-образа` , который запустит программу внутри контейнера, которая выведет в терминал текст, который мы сейчас читаем.
4. Служба `Docker daemon` передает этот вывод `Docker client` , который отправляет его в наш терминал.

## Шаг 2 Настройка установленного Docker и его компонент

Можно было бы перейти в шагу 3 по установке `nvidia-docker` , но лучше сразу выполним некоторую дополнительную настройку которая сделает использование `Docker` и его компонент удобнее для нас.

Установим запуск `Docker` сразу при загрузке ОС, выполнив команды:

```
sudo systemctl enable docker.service  
sudo systemctl enable containerd.service
```

По умолчанию действия с `Docker` осуществляет администратор системы, и нам постоянно нужно будет использовать команду `sudo`, чтобы выполнять команды с `Docker` от его имени. Можно сделать настройку, чтобы мы могли запускать `Docker` от имени нашего пользователя.

Довольно стандартной настройкой является создание группы пользователей `Docker` и добавление нашего пользователя в эту группу. Потенциально это несколько снижает безопасность нашей системы и следует обсудить использование данной настройки с коллегами.

Для реализации выполняем данные команды:

Создаем группу пользователей `docker`  

```
sudo groupadd docker  
```

  
добавляем нашего пользователя в эту группу  

```
sudo usermod -aG docker $USER
```  
  
Активируем изменения  

```
newgrp docker`
```


После этих настроек мы сможем запустить тот же контейнер `hello-world` уже без `sudo` . Убедимся в этом, выполнив команду:


```
docker run hello-world
```


В этот раз сообщений о скачивании `Docker`-образа (`Pulling image`) уже не будет (мы скачали его ранее). И мы сразу увидим в терминале сообщение от `Docker`-контейнера:


```
Hello from Docker!  
This message shows that your installation appears to be working correctly.  
  
To generate this message, Docker took the following steps:  
 1. The Docker client contacted the Docker daemon.  
 2. The Docker daemon pulled the "hello-world" image from the Docker Hub.  
    (amd64)  
 3. The Docker daemon created a new container from that image which runs the  
    executable that produces the output you are currently reading.  
 4. The Docker daemon streamed that output to the Docker client, which sent it  
    to your terminal.  
  
To try something more ambitious, you can run an Ubuntu container with:  
 $ docker run -it ubuntu bash  
  
Share images, automate workflows, and more with a free Docker ID:  
 https://hub.docker.com/  
  
For more examples and ideas, visit:  
 https://docs.docker.com/get-started/
```


Переходим к следующему шагу.

## Шаг 3 Установить `nvidia-docker` поверх `Docker`

Для выполнения данного шага выполним инструкции указанные в официальной документации nvidia([ссылка](https://docs.nvidia.com/datacenter/cloud-native/container-toolkit/install-guide.html)).

добавим GPG -ключ  
```
curl -s -L https://nvidia.github.io/nvidia-docker/gpgkey | \  
  sudo apt-key add -  
```
  
Добавим дополнительные репозитории  
```
distribution=$(. /etc/os-release;echo $ID$VERSION_ID)  
curl -s -L \  
    https://nvidia.github.io/nvidia-docker/$distribution/nvidia-docker.list | \  
    sudo tee /etc/apt/sources.list.d/nvidia-docker.list  
```
  
Обновляем список пакетов для apt и устанавливаем nvidia-docker2  

```
sudo apt update && sudo apt install -y nvidia-docker2  
```

  
Перезапускаем службу Docker daemon, чтобы применить изменения  

```
sudo systemctl restart docker
```


Интересно отметить, что при выполнении команды:


```
distribution=$(. /etc/os-release;echo $ID$VERSION_ID)  
curl -s -L \  
    https://nvidia.github.io/nvidia-docker/$distribution/nvidia-docker.list | \  
    sudo tee /etc/apt/sources.list.d/nvidia-docker.list
```


Будет создан файл `/etc/apt/sources.list.d/nvidia-docker.list` . Если мы откроем этот файл на просмотр выполнив команду:


```
cat /etc/apt/sources.list.d/nvidia-docker.list
```


Мы увидим, что несмотря на версию нашего дистрибутива `Ubuntu 20.04` все URL-ссылки будут на репозитории `nvidia-docker` для `Ubuntu 18.04`


```
deb https://nvidia.github.io/libnvidia-container/stable/ubuntu18.04/$(ARCH) /  
# deb https://nvidia.github.io/libnvidia-container/experimental/ubuntu18.04/$(ARCH) /  
deb https://nvidia.github.io/nvidia-container-runtime/stable/ubuntu18.04/$(ARCH) /  
# deb https://nvidia.github.io/nvidia-container-runtime/experimental/ubuntu18.04/$(ARCH) /  
deb https://nvidia.github.io/nvidia-docker/ubuntu18.04/$(ARCH) /
```


В официальной документации nVidia пишет, что так и должно быть ([ссылка](https://docs.nvidia.com/datacenter/cloud-native/container-toolkit/install-guide.html#setting-up-nvidia-container-toolkit)).

Теперь выполним “hello-world” от `nvidia-docker` , — запустим `Docker-контейнер` `nvidia/cuda:11.6.2-base-ubuntu20.04` и внутри него выполним команду `nvidia-smi` , которая покажет нам информацию о доступны внутри контейнера nVidia GPU.

Для этого выполним команду:


```
docker run --rm --runtime=nvidia --gpus all \  
    nvidia/cuda:11.6.2-base-ubuntu20.04 nvidia-smi
```


Если мы получили ее вывод в терминал

```

Thu May  4 05:18:01 2023         
+-----------------------------------------------------------------------------+  
| NVIDIA-SMI 515.86.01    Driver Version: 515.86.01    CUDA Version: 11.7     |  
|-------------------------------+----------------------+----------------------+  
| GPU  Name        Persistence-M| Bus-Id        Disp.A | Volatile Uncorr. ECC |  
| Fan  Temp  Perf  Pwr:Usage/Cap|         Memory-Usage | GPU-Util  Compute M. |  
|                               |                      |               MIG M. |  
|===============================+======================+======================|  
|   0  NVIDIA A10          Off  | 00000000:01:00.0 Off |                  Off |  
|  0%   54C    P0    62W / 150W |   3195MiB / 24564MiB |      0%      Default |  
|                               |                      |                  N/A |  
+-------------------------------+----------------------+----------------------+  
                                                                                 
+-----------------------------------------------------------------------------+  
| Processes:                                                                  |  
|  GPU   GI   CI        PID   Type   Process name                  GPU Memory |  
|        ID   ID                                                   Usage      |  
|=============================================================================|  
+-----------------------------------------------------------------------------+

```

то `nvidia-docker` установлен корректно.

**Примечание:** иногда при такой проверке так же может возникнуть сообщение о том, что nVidia GPU не обнаружен. Часто это возникает если мы устанавливали драйвер для nVidia GPU с официального сайта Nvidia и выполняли последующую сборку из исходников.

Нам нужно будет просто переустановить драйвер на нашу nVidia GPU ([ссылка](https://docs.nvidia.com/datacenter/cloud-native/container-toolkit/install-guide.html#nvidia-drivers)). Перезагружаемся, уже установленный ранее нами `nvidia-docker` сам найдет nVidia GPU в системе, специально что то делать для этого не нужно.  
Снова перепроверяем `nvidia-docker` запуском тестового контейнера, - все должно заработать.

# Создадим `docker-`образ на основе образов от nVidia с поддержкой вычислений на GPU

Для данной статьи я подготовил `git-hub` репозиторий ([ссылка](https://github.com/medphisiker/docker_yolov8)).

Клонируем его с помощью `git`, выполнив команду:


```
git clone git@github.com:medphisiker/docker_yolov8.git
```


Перейдем в него:


```
cd docker_yolov8
```


Процесс создания своего docker-образа включает 2 этапа:

- Создаем `Dockerfile`. Это файл в котором содержатся команды для создания образа.
- Выполняем сборку `docker-образа` командой `docker build`

Еще мы можем опубликовать созданный нами `docker-образ` в некотором реестре `docker-образов` , например на `Docker Hub`. Тогда другие разработчики могли бы использовать его. Наш образ учебный, и не несет большой ценности, поэтому мы не будем этого делать.

Рассмотрим `Dockerfile` для создания нашего контейнера

Рассмотрим наш `.devcontainer/Dockerfile`

```

|   |
|---|
|# образ для контейнера|
||FROM nvcr.io/nvidia/tensorrt:23.03-py3|
|||
||# отключаем интерактивные вопросы при установке пакетов|
||# при создании docker-контейнера|
||ARG DEBIAN_FRONTEND=noninteractive|
|||
||# установим opencv-python|
||# для этого нам понадобится ее зависимость libgl1|
||# см. https://stackoverflow.com/a/68666500|
||RUN apt-get update && apt-get install -y python3-opencv|
||RUN pip install opencv-python==4.6.0.66|
|||
||# Установим PyTorch 1.12 и Cuda 1.13|
||RUN pip install torch==1.12.0+cu113 torchvision==0.13.0+cu113 \|
||torchaudio==0.12.0 --extra-index-url https://download.pytorch.org/whl/cu113|
|||
||# Установим ultralytics yolov8|
||RUN pip install ultralytics==8.0.99|
|||
||# Зависимости для экспорта yolov8 в ONNX|
||RUN pip install onnx==1.14.0 onnxsim==0.4.28 install onnxruntime-gpu==1.14.1 \|
||rich==13.3.5 flatbuffers==23.5.9 sympy==1.12 coloredlogs==15.0.1 \|
||humanfriendly==10.0 markdown_it_py==2.2.0 mpmath==1.3.0 mdurl==0.1.2|
|||
||# установим kernel для Jupyter Notebook|
||RUN pip install ipykernel==6.23.0|
|||
||# чистим систему от лишнего|
||RUN apt-get clean && apt-get autoremove|
|||
||# папка для подключения репозитория с кодом|
||WORKDIR /workspace|
```
Команда `FROM` — задаёт базовый (родительский) образ. `Docker-образы` создаются на основе других `Docker-образов` , довольно часто подобным выступает `ubuntu:20.04`([ссылка](https://hub.docker.com/_/ubuntu)).

# каталог Docker-образов nVidia
nVidia имеет свой собственный каталог `Docker-образов` для самых разных задач NGC ([ссылка](https://catalog.ngc.nvidia.com/containers)).

В нашем случае для поддержки nVidia GPU мы будем брать в качестве базового образа либо образы `nvidia/cuda` с `Docker Hub`([ссылка](https://hub.docker.com/r/nvidia/cuda/tags)), например

образ для контейнера  

```
FROM nvidia/cuda:11.6.0-base-ubuntu20.04
```


Либо если нам нужна поддержка `TensorRT`([ссылка](https://developer.nvidia.com/tensorrt)) то мы можем использовать образы `nvidia/tensorrt` ([ссылка](https://catalog.ngc.nvidia.com/orgs/nvidia/containers/tensorrt)), например

образ для контейнера  

```
FROM nvcr.io/nvidia/tensorrt:23.03-py3
```


`NVIDIA® TensorRT™` это специальный SDK для высокопроизводительного инференса моделей глубокого обучения, включающая в себя оптимизатор инференса на конкретном исполняющем устройстве (например нашей nVidia GPU A10 или Jetson Nano) и специальное окружение для быстрого запуска и высокой пропускной способности.

С одной стороны `TensorRT` , преобразует вычислительный граф нашей нейросети в последовательность инструкций поддерживаемых нашей GPU, с другой выполнит различные оптимизации нашей модели для максимально эффективного запуска нейросети на данном устройстве.
	

Команда `ARG` — задаёт переменные для передачи Docker во время сборки образа. Некоторые пакеты будут требовать ввода от пользователя, чтобы этого избежать мы будет устанавливать их не в интерактивном режиме.

отключаем интерактивные вопросы при установке пакетов  
при создании docker-контейнера  

```
ARG DEBIAN_FRONTEND=noninteractive
```


Команда `RUN` — выполняет команду и создаёт слой образа. Используется для установки различных пакетов в контейнер, при этом это те же команды которые бы мы выполняли при их установке в непосредственно в нашу ОС Linux:

обновляем список пакетов  

```
RUN apt-get update  
```

  
Установим PyTorch 1.12 и Cuda 1.13  

```docker
RUN pip install torch==1.12.0+cu113 torchvision==0.13.0+cu113 torchaudio==0.12.0 --extra-index-url https://download.pytorch.org/whl/cu113  
```

  
Установим ultralytics yolov8  

```
RUN pip install ultralytics  
```

  
установим opencv-python  
для этого нам понадобится ее зависимость libgl1  
см. https://stackoverflow.com/a/68666500  

```
RUN apt-get update && apt-get install -y python3-opencv  
RUN pip install opencv-python==4.6.0.66  
```

  
установим kernel для Jupyter Notebook  

```
RUN pip install ipykernel  
```

  
чистим систему от лишнего  


```
RUN apt-get clean && apt-get autoremove
```


Последняя команда `WORKDIR` — задаёт рабочую директорию для всех выполняемых инструкций. При запуске `Docker-контейнера` в режиме интерактивной консоли, мы сразу попадем в данный каталог.

папка для подключения репозитория с кодом  

```
WORKDIR /workspace
```


Выполняем сборку `docker-образа` командой `docker build`

Согласно описанию репозитория ([ссылка](https://github.com/medphisiker/docker_yolov8)), переходим в каталог `/.devcontainer` , выполнив команду:


```
cd .devcontainer
```


И затем выполняем команду для сборки `Docker-образа` :


```
docker build -t pytorch_yolov8:0.0.1 .
```


В терминале, мы увидим вывод о скачивании базового `Docker-образа` `nvcr.io/nvidia/tensorrt:23.03-py3` на основе которого будет создан наш, а затем и вывод отражающий весь процесс сборки. По окончанию процесса мы увидим сообщение о том, что наш образ собран

```

 => exporting to image                                                                                                                      0.0s  
 => => exporting layers                                                                                                                     0.0s  
 => => writing image sha256:e33e6ee69cd7ed5afb9eb3de8d1a4f7013031639378abd9b4bc96393698b7ece                                                0.0s  
 => => naming to docker.io/library/pytorch_yolov8:0.0.1
```


Образ `pytorch_yolov8:0.0.1` собран и доступен для запуска docker из любого каталога
Запуск `docker-образа` pytorch_yolov8

Согласно описанию репозитория ([ссылка](https://github.com/medphisiker/docker_yolov8)), теперь нам нужно перейти в каталог репозитория, выполним команду:


```
cd ..
```


Теперь мы можем выполнив команду для запуска `docker-контейнера pytorch_yolov8`:


```
docker run \  
--gpus all \  
--rm \  
-it \  
-v ./yolov8:/workspace \  
pytorch_yolov8:0.0.1
```


```

 docker run --gpus all --rm -it -v ./yolov8:/workspace pytorch_yolov8:0.0.1

```

`Docker-образ` содержит рабочее окружение (все необходимые зависимости) для запуска `ultralytics yolov8` — `python`-скриптов, `Jupyter Notebook’ов` и нейронных сетей семейства `YOLOv8`.

Но самих скриптов и файлов нейронных сетей в нем нет.

Поэтому при запуске контейнера, к нему нужно примонтировать папку `./yolov8` с кодом.

Поскольку наш образ основан на образе от `nvidia/tensorrt` ([ссылка](https://catalog.ngc.nvidia.com/orgs/nvidia/containers/tensorrt)), при своем запуске он выдаст информационное сообщение от компании nVidia:


```
=====================  
== NVIDIA TensorRT ==  
=====================  
  
NVIDIA Release 23.03 (build 54538654)  
NVIDIA TensorRT Version 8.5.3  
Copyright (c) 2016-2023, NVIDIA CORPORATION & AFFILIATES. All rights reserved.  
  
Container image Copyright (c) 2023, NVIDIA CORPORATION & AFFILIATES. All rights reserved.  
  
https://developer.nvidia.com/tensorrt  
  
Various files include modifications (c) NVIDIA CORPORATION & AFFILIATES.  All rights reserved.  
  
This container image and its contents are governed by the NVIDIA Deep Learning Container License.  
By pulling and using the container, you accept the terms and conditions of this license:  
https://developer.nvidia.com/ngc/nvidia-deep-learning-container-license  
  
To install Python sample dependencies, run /opt/tensorrt/python/python_setup.sh  
  
To install the open-source samples corresponding to this TensorRT release version  
run /opt/tensorrt/install_opensource.sh.  To build the open source parsers,  
plugins, and samples for current top-of-tree on master or a different branch,  
run /opt/tensorrt/install_opensource.sh -b <branch>  
See https://github.com/NVIDIA/TensorRT for more information.
```


Поскольку при создании `Docker-образа` мы указывали `WORKDIR /workspace` , терминал запущенный внутри контейнера будет запущен в этом каталоге:


```
root@3a20fb4eafe6:/workspace# 
```


Разные версии CUDA в нашей ОС и запущенном `Docker-контейнере`

Выполним команду `nvidia-smi` в терминале нашей ОС

и в терминале внутри `Docker-контейнера`

Мы видим, что `nVidia GPU A10` с драйвером `515.86.01` доступа и там и там. При этом в нашей ОС стоит `CUDA 11.7`, а в `Docker-контейнере` `CUDA 12.1` . Еще одно дополнительное удобство в том, что мы можем использовать внутри `Docker-контейнера` `CUDA` версии отличной от той, что стоит в нашей системе.

Это очень удобно для испытания новых функций или запуска последних версий фреймворков глубокого обучения требующих самой последней версии `CUDA` для своей работы.

Тестирование доступности вычислений на nVidia GPU для PyTorch

При запуске контейнера мы примонтировали папку `./yolov8` в каталог внутри контейнера `/workspace` . Внутри данного каталога находятся следующие файлы:

```
root@3a20fb4eafe6:/workspace# ls -la  
total 136  
drwxrwxr-x 2 1000 1000   4096 May  4 07:55 .  
drwxr-xr-x 1 root root   4096 May  4 07:24 ..  
-rw-rw-r-- 1 1000 1000 124933 May  4 06:16 YOLOv8_segment.ipynb  
-rw-rw-r-- 1 1000 1000    639 May  4 07:55 pytorch_gpu_test.py
```


Файл `pytorch_gpu_test.py` вызывает PyTorch, выводит его версию и CUDA, выводит порядковый номер текущего устройства CUDA, название текущего устройства CUDA.

Так же он создает два случайных вектора, отправляет их на вычислительное устройство CUDA и перемножает их.

Выполним данный код в консоли:


```
python pytorch_gpu_test.py
```


Получим вывод:


```
Версия PyTorch, - 1.12.0+cu113  
Доступность устройства CUDA для вычислений, -  True  
Номер текущего устройство CUDA, - 0  
Название текущего CUDA-устройства, - NVIDIA A10  
  
Тест вычислений на текущем CUDA-устройстве  
  
Вектор t1 tensor([[-0.4089,  0.2766]], device='cuda:0')  
Вектор t2 tensor([[-2.2404,  0.6002]], device='cuda:0')  
Вектор t3 = t1 * t2 tensor([[0.9161, 0.1660]], device='cuda:0')
```


Если все прошло успешно, `PyTorch` может использовать nVidia GPU в своих расчетах.

В целом обычно инференс моделей глубокое обучения и происходит запуском некоторого `python`-скрипта внутри `Docker-контейнера` который их использует.
# Remote-SSH
Однако, у IDE `VS code` есть интересная особенность. Для работы с удаленным сервером VS code (расширение `Remote-SSH` ([ссылка](https://marketplace.visualstudio.com/items?itemName=ms-vscode-remote.remote-ssh))) может установить на него `VS code server`. `VS code server` это `VS code` без графического интерфейса пользователя к которому может подключится `VS code` на нашем локальном компьютере по SSH. При этом мы сможем работать с удаленным сервером в интерфейсе VS code на нашем компьютере.

При этом удаленным компьютером может быть:

- реальный удаленный сервер (расширение `Remote-SSH` ([ссылка](https://marketplace.visualstudio.com/items?itemName=ms-vscode-remote.remote-ssh)))
- запущенный docker-контейнер (расширения `Dev Containers` ([ссылка](https://marketplace.visualstudio.com/items?itemName=ms-vscode-remote.remote-containers)) и Docker([ссылка](https://marketplace.visualstudio.com/items?itemName=ms-azuretools.vscode-docker)))
- Linux в WSL (расширение `WSL` ([ссылка](https://marketplace.visualstudio.com/items?itemName=ms-vscode-remote.remote-ssh)))

`VS code` при установке расширения `Python` ([ссылка](https://marketplace.visualstudio.com/items?itemName=ms-python.python)) поддерживает работу с `Jupyter-Notebook’ами`.

Благодаря этому, мы можем использовать `Docker-контейнер` не только для упаковки готового решения для заказчика, но и непосредственно для разработки и обучения моделей.

# Подключение VS Code к Docker-контейнеру

Подключаем `VS code` к запущенному нами `Docker-контейнеру` как показано на видео ниже
<iframe width="560" height="315" src="https://www.youtube.com/embed/G2Ha4cDFb48?si=H31RE1XMbTlVvQoQ" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

Теперь наш `VS code` подключен к `Docker-контейнеру` . Установим в `VS code server` внутри контейнера расширение `Python` ([ссылка](https://marketplace.visualstudio.com/items?itemName=ms-python.python)).
<iframe width="560" height="315" src="https://www.youtube.com/embed/fRfoPn43Kts?si=fqEO3GqHMp5oohtb" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
Теперь мы может открыть `Jupyter-Notebook` `YOLOv8_segment.ipynb` и работать с ним привычным нам образом.

Мы видим, что проверка от `ultralytics yolov8` прошла успешно в нашем контейнере ей доступно устройство CUDA:0 (NVIDIA A10)

Ultralytics YOLOv8.0.91 🚀 Python-3.8.10 torch-1.12.0+cu113 CUDA:0 (NVIDIA A10, 24119MiB)  
Setup complete ✅ (12 CPUs, 31.1 GB RAM, 327.3/913.8 GB disk)

Изначально наш `Jupyter-Notebook` запущен в каталоге `/` , проверим выполнив ячейку в ноутбуке:

изначально наш ноутбук запущен в каталоге '/'  
import os  
  
print(os.getcwd()) # выведет 

Изменим на наш рабочий каталог, выполнив ячейку в ноутбуке:

# изменим на нужный нам  
os.chdir('/workspace')  
print(os.getcwd()) # выведет /workspace

Скачаем картинки на которых будет тестировать нейросеть, выполнив ячейку в ноутбуке:

# скачиваем картинку с автобусом и Зиданом  
!wget https://ultralytics.com/images/zidane.jpg  
!wget https://ultralytics.com/images/bus.jpg

В каталоге `/workspace` появится две картинки (`bus.jpg` и `zidane.jpg`):

root@3a20fb4eafe6:/workspace# ls -la  
total 768  
drwxrwxr-x 2 1000 1000   4096 May  4 09:13 .  
drwxr-xr-x 1 root root   4096 May  4 07:24 ..  
-rw-rw-r-- 1 1000 1000 108991 May  4 09:10 YOLOv8_segment.ipynb  
-rw-r--r-- 1 root root 487438 May  4 09:00 bus.jpg  
-rw-rw-r-- 1 1000 1000    649 May  4 08:07 pytorch_gpu_test.py  
-rw-r--r-- 1 root root 168949 May  4 09:00 zidane.jpg

Картинка `bus.jpg` выгледит так:

![](https://miro.medium.com/v2/resize:fit:1050/1*X-sY5eDoWVa7W9nsgL3MJg.jpeg)

Теперь запустим нейросеть для предсказания масок на `bus.jpg` , выполнив ячейку в ноутбуке

# выполним инференс на картинки через интерфейс командной строки  
# скачивать файл модели не нужно, если его нет локально команда   
# сама его скачает  
# device=cpu модельбудет запущена на CPU  
!yolo segment predict model=yolov8l-seg.pt source=bus.jpg device=cpu

Мы выполняли предсказание на CPU, поэтому получилось не очень быстро:

Downloading https://github.com/ultralytics/assets/releases/download/v0.0.0/yolov8l-seg.pt to yolov8l-seg.pt...  
100%|██████████████████████████████████████| 88.1M/88.1M [00:36<00:00, 2.51MB/s]  
Ultralytics YOLOv8.0.91 🚀 Python-3.8.10 torch-1.12.0+cu113 CPU  
YOLOv8l-seg summary (fused): 295 layers, 45973568 parameters, 0 gradients, 220.5 GFLOPs  
  
image 1/1 /workspace/bus.jpg: 640x480 5 persons, 1 bus, 1 stop sign, 1 tie, 436.1ms  
Speed: 1.5ms preprocess, 436.1ms inference, 1.5ms postprocess per image at shape (1, 3, 640, 640)  
Results saved to runs/segment/predict

Время инференса модели составляет, — 436.1ms.

Виуализация предсказаний на картинке расположена в каталоге `runs/segment/predict` , посмотрим на результат.

![](https://miro.medium.com/v2/resize:fit:1050/1*Q0pQjFyWxdzgfNMmIq-ePg.jpeg)

Теперь запустим нейросеть для предсказания масок на `bus.jpg` , но уже используя нашу GPU, выполнив ячейку в ноутбуке

# device=0 означает устройство CUDA под порядковым номером 0  
!yolo segment predict model=yolov8l-seg.pt source=bus.jpg device=0

На GPU инференс осуществился значительно быстрее, за 12.1ms, в 36 раз быстрее.

Ultralytics YOLOv8.0.91 🚀 Python-3.8.10 torch-1.12.0+cu113 CUDA:0 (NVIDIA A10, 24119MiB)  
YOLOv8l-seg summary (fused): 295 layers, 45973568 parameters, 0 gradients, 220.5 GFLOPs  
  
image 1/1 /workspace/bus.jpg: 640x480 5 persons, 1 bus, 1 stop sign, 1 tie, 12.1ms  
Speed: 1.8ms preprocess, 12.1ms inference, 1.1ms postprocess per image at shape (1, 3, 640, 640)  
Results saved to runs/segment/predict2

Преобразуем нашу модель для запуска на `TensorRT`, выполнив ячейку ноутбука

# экспортируем модель в TensorRT  
!yolo mode=export model=yolov8l-seg.pt format=engine device=0

Видим, что вначале модель будет преобразуется из модели `PyTorch` (`yolov8l-seg.pt`) в обобщенный формат `ONNX` .

PyTorch: starting from yolov8l-seg.pt with input shape (1, 3, 640, 640) BCHW and output shape(s) ((1, 116, 8400), (1, 32, 160, 160)) (88.1 MB)  
  
ONNX: starting export with onnx 1.13.1 opset 10...  
ONNX: simplifying with onnxsim 0.4.28...  
ONNX: export success ✅ 5.0s, saved as yolov8l-seg.onnx (175.5 MB)

В результате мы получим модель `yolov8l-seg.onnx` (175.5 MB).

Уже после этого модель `yolov8l-seg.onnx` будет оптимизироваться `TensorRT` для максимальной производительности на нашей nVidia GPU.

TensorRT: starting export with TensorRT 8.5.3.1...  
[05/04/2023-09:25:56] [TRT] [I] [MemUsageChange] Init CUDA: CPU +556, GPU +0, now: CPU 6074, GPU 5833 (MiB)  
[05/04/2023-09:25:57] [TRT] [I] [MemUsageChange] Init builder kernel library: CPU +542, GPU +120, now: CPU 6670, GPU 5953 (MiB)  
[05/04/2023-09:25:57] [TRT] [W] CUDA lazy loading is not enabled. Enabling it can significantly reduce device memory usage. See `CUDA_MODULE_LOADING` in https://docs.nvidia.com/cuda/cuda-c-programming-guide/index.html#env-vars  
[05/04/2023-09:25:57] [TRT] [I] ----------------------------------------------------------------  
[05/04/2023-09:25:57] [TRT] [I] Input filename:   yolov8l-seg.onnx  
[05/04/2023-09:25:57] [TRT] [I] ONNX IR version:  0.0.5  
[05/04/2023-09:25:57] [TRT] [I] Opset version:    10  
[05/04/2023-09:25:57] [TRT] [I] Producer name:    pytorch  
[05/04/2023-09:25:57] [TRT] [I] Producer version: 1.12.0  
[05/04/2023-09:25:57] [TRT] [I] Domain:             
[05/04/2023-09:25:57] [TRT] [I] Model version:    0  
[05/04/2023-09:25:57] [TRT] [I] Doc string:         
[05/04/2023-09:25:57] [TRT] [I] ----------------------------------------------------------------  
[05/04/2023-09:25:57] [TRT] [W] onnx2trt_utils.cpp:377: Your ONNX model has been generated with INT64 weights, while TensorRT does not natively support INT64. Attempting to cast down to INT32.  
TensorRT: input "images" with shape(1, 3, 640, 640) DataType.FLOAT

В результате мы получим модель `yolov8l-seg.engine` (184 MB). Теперь запустим нейросеть для предсказания масок на `bus.jpg` , но уже используя нашу GPU и модель `yolov8l-seg.engine`, выполнив ячейку в ноутбуке

# выполним предсказание модель с помощью TensorRT  
!yolo segment predict model=yolov8l-seg.engine imgsz=640 source=bus.jpg

Получим следующий результат

Ultralytics YOLOv8.0.91 🚀 Python-3.8.10 torch-1.12.0+cu113 CUDA:0 (NVIDIA A10, 24119MiB)  
Loading yolov8l-seg.engine for TensorRT inference...  
[05/04/2023-09:34:33] [TRT] [I] Loaded engine size: 183 MiB  
[05/04/2023-09:34:34] [TRT] [I] [MemUsageChange] Init cuDNN: CPU +1538, GPU +436, now: CPU 2439, GPU 4215 (MiB)  
[05/04/2023-09:34:34] [TRT] [W] TensorRT was linked against cuDNN 8.6.0 but loaded cuDNN 8.3.2  
[05/04/2023-09:34:34] [TRT] [I] [MemUsageChange] TensorRT-managed allocation in engine deserialization: CPU +0, GPU +181, now: CPU 0, GPU 181 (MiB)  
[05/04/2023-09:34:34] [TRT] [I] [MemUsageChange] Init cuDNN: CPU +0, GPU +32, now: CPU 2255, GPU 4215 (MiB)  
[05/04/2023-09:34:34] [TRT] [W] TensorRT was linked against cuDNN 8.6.0 but loaded cuDNN 8.3.2  
[05/04/2023-09:34:34] [TRT] [I] [MemUsageChange] TensorRT-managed allocation in IExecutionContext creation: CPU +0, GPU +99, now: CPU 0, GPU 280 (MiB)  
[05/04/2023-09:34:34] [TRT] [W] CUDA lazy loading is not enabled. Enabling it can significantly reduce device memory usage. See `CUDA_MODULE_LOADING` in https://docs.nvidia.com/cuda/cuda-c-programming-guide/index.html#env-vars  
  
WARNING ⚠️ Source shapes differ. For optimal performance supply similarly-shaped sources.  
image 1/1 /workspace/bus.jpg: 640x640 4 persons, 1 bus, 1 tie, 10.3ms  
Speed: 2.4ms preprocess, 10.3ms inference, 2.2ms postprocess per image at shape (1, 3, 640, 640)  
Results saved to runs/segment/predict3

Инференс модели составил уже 10.3ms, что на `~15%` быстрее.

**Итоги:**

- мы познакомились с `Docker`
- установили `nvidia-docker`, предоставляющего нам возможность использования nVidia GPU внутри запущенных контейнеров
- создали наш первый `Docker-образ` с поддержкой вычислений на nVidia GPU
- запустили `Docker-контейнер` и убедились в доступности nVidia GPU для расчетов которые выполняет PyTorch
- подключили `VS code` к запущенному `Docker-контейнеру` , и начали работать с `Jupyter Notebook` привычным для нас образом
- запустили внутри `Docker-контейнера` нейросеть для instance-сегментации `yolov8l-seg` на CPU и GPU
- преобразовали нейросеть `yolov8l-seg` из формата `PyTorch` в `TensorRT` через `ONNX`, и получили ~15% ускорения на инференсе.

Надеюсь эта статья помогла вам в запуске первого тестового `Docker-контейнера` использующего nvidia GPU для задач машинного обучения и вам было интересно. Впереди у вас множество увлекательных экспериментов.