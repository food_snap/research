# Дизайн-документ для проекта системы определения пищевой ценности по фото при помощи компьютерного зрения "FoodSnap"


## Оглавление:
- [1. Цели и задачи проекта](#1-цели-и-задачи-проекта)
- [2. Пользовательский анализ](#2-пользовательский-анализ)
- [3. Оценка рынка](#3-оценка-рынка)
- [4. Бизнес-требования и ограничения](#4-бизнес-требования-и-ограничения)
- [5. Архитектура системы](#5-архитектура-системы)
- [6. План разработки](#7-план-разработки)

## 1. Цели и задачи проекта
### 1.1. Назначение проекта
Назначение проекта заключается в создании цифрового сервиса поддержки пользователя, помогающего в подсчёте усваиваемых калорий и полезных веществ по передаваемым пользователем фото потребляемой пищи.
Данная информация необходима для контроля веса и в целом для организации здорового питания клиента.

### 1.2. Обоснование проекта

На данный момент среди сторонников здорового образа жизни популярна практика удалённого консультирования у диетологов. Смысл заключается в том, что клиент отправляет диетологу фото потребляемой пищи, последний же, в свою очередь, подсчитывает примерный состав БЖУ и калорийность. На основе данной информации консультант даёт советы по корректировке личной диеты.\
Стоимость данных услуг ограничивает возможность каждому желающему получать необходимую ему консультацию.\
Однако современные технологии позволяют создать цифровой сервис, решающий данные задачи и в то же время доступный практически каждому, у кого есть выход в интернет.
### 1.3 Задачи проекта
- **Распознавание фото**\
Так как на данный момент наилучшим средством распознавания изображений являются нейронные сети, то основной задачей проекта является обучение нейронной сети определять блюда и/или продукты, представленные на фото;
- **Определение веса продукта**\
Необходимо разработать методы определения веса продуктов;
- **Подсчёт калорий**\
Для подсчёта калорий необходимо разработать соответствующий алгоритм и создать СУБД для хранения данных о составе блюд, а также о пищевой и энергетической ценности продуктов, из которых они состоят;
- **Создание интерфейса пользователя**\
Необходимо разработать удобный интерфейс, обеспечивающий взаимодействие пользователя с системой.


## 2. Пользовательский анализ
### 2.1 Потребности и проблемы пользователей
1. Большинству людей сложно найти время и силы подсчитывать калории самостоятельно, а тем более точно;
2. Нужно иметь опыт и практику подсчета калорий , которую порой надо нарабатывать, а учителя нет;
3. Трудно записывать дневной рацион, а уж тем более разделять его на составляющие;
4. Трудно придерживаться плана диеты;
5. Люди порой не понимают, что они едят;
6. Недостаток внимания и мотивации;
7. Отсутствие качественных инструментов и/или их дороговизна (это касается как приложений, так и приборов);
8. Люди стремятся к быстрому и контролируемому результату;
9. Социум оказывает давление из-за которого люди стремятся выглядеть лучше, стройнее, мускулистей.

### 2.2 Целевая аудитория
- Возраст: 18-45 лет. Этот диапазон выбран неспроста, ведь мы не хотим иметь юридических последствий и нести ответственность за людей младше 18 лет, а люди примерно с 45 лет уже не так интересуются цифровыми товарами.
- Пол: мужской и женский, однако перевес будет скорее в сторону женщин, так как они более строго придерживаются диеты, а также создают больше фотографий (в том числе и еды).
- Мотивация:
  - Поддержка и улучшение здоровья (стремление улучшить качество жизни);
  - Набор мышечной массы (люди могут стесняться своего худощавого тела);
  - Снижение излишней массы (стремление снизить жировую прослойку).
- Интересы:
  - Занимаются спортом / ходят в тренажерные залы;
  - Используют приборы, приложения и сервисы для спорта и питания;
  - Склонны пробовать новые технические решения для улучшения своего образа жизни.
- Экономические особенности:
  - Доход средний и выше среднего. Склонны больше тратить деньги на приложения и специальное питание. Могут выбирать что есть, а не довольствоваться тем, что могут купить на минимальную оплату труда.

### 2.3 Места поиска респондентов для качественных и количественных исследований
- Спортзалы и фитнес-центры;
- Фитнес-классы и семинары (йога, пилатес, групповые тренировки и прочее);
- Кафе, рестораны, магазины спортивного питания и прилавки со здоровым питанием;
- Профильные группы в социальных сетях, форумы и платформы;
- Мероприятия с тематикой здорового питания;
- Заказ опросов в специальных приложениях за деньги.

### 2.4 Плюсы и минусы решения
- Плюсы:
  - Удобство быстрой загрузки и обработки фото по сравнению с самостоятельным подсчётом;
  - Дополнительная мотивация при помощи визуализации и расширенного анализа;
  - Точность оценки (хоть сервис и оценивает приблизительно - это может оказаться гораздо точнее человека);
  - Пользователи смогут больше узнать о том, что они едят и таким образом стать более образованными в этом направлении.
- Минусы:
  - Пользователи боятся предоставлять свои реальные данные не проверенным сервисам;
  - Если сервис будет часто ошибаться, то это может отпугнуть пользователей и перевести сервис в разряд "игрушки на один раз";
  - Сложность фотографировать каждый прием пищи;
  - Стоимость подписки на сервис (это сократит аудиторию).

### 2.5 Альтернативы
- Другие приложения по подсчету калорий, которые мало того, что имеют больший функционал, так еще и зарекомендовали себя как популярный и надежный сервис;
- Фитнес-трекеры, которые уже в состоянии оценивать и питание;
- Консультанты-диетологи;
- Книги и таблицы калорийности;
- EXCEL / Google Sheets.

## 3. Оценка рынка
### 3.1 Основные метрики
- на 2023 год насчитывается 700 млн активных пользователей телеграм. Доля РФ составляет 68%, где 58% - это мужчины, а 42% - это женщины;
- 59% людей в той или иной степени следят за питанием;
- То есть мы можем говорить о цифре примерно в 280 млн потенциальных пользователей. Однако стоит учесть, что это очень оптимистичная оценка и принято ее умножать на 0.1 или даже на 0.01. Таким образом получаем около 2.8 млн человек, что выглядит правдоподобнее.
- Стоимость платной рекламы может варьироваться от 100 до 1000 руб. за 1 000 показов.
- Если мы вставим цену примерно в 500 р./мес, то при 10 000 активных пользователей в месяц мы получим 5 000 000, но стоит учитывать следующие факторы, влияющие на конечный доход, которые можно узнать только при тестовой проливке трафика:
Воронка конверсий значительно снизит количество платящих пользователей. Допустим мы показали рекламу 10 000 людей и 15% из них пришло на сервис, а конверсия в регистрацию и заполнение профиля будет тоже 15%, то на выходе мы получим 10000 -> 1500 -> 225 пользователей, из которых купят подписку около 15%. Таким образом мы получим около 30 человек с подпиской. При идеальном исходе (когда люди не отменяют подписку) мы получим 30 * 500 = 15000 руб. при затратах 10000 / 1000 * 500= 5000 руб. Это очень оптимистичный, но сильно приближенный к реальности расчет.

### 3.1 Дополнительные сведения
- Сезонность: как правило люди интересуются словом "диета" начиная с января. В этот момент, за январь и февраль, происходит взрывной рост популярности этого запроса. Далее интерес равномерно снижается до нового года, что дает нам некую сезонность. Люди хотят похудеть к лету.
- Тренды: на данный момент наблюдаются тренды на кето-диеты, интервальное голодание и это может влиять на интерес к нашему сервису.
- Партнёрство: можно наладить сотрудничество с тренерами, может даже оказаться, что тренера будут использовать наше приложение зарабатывая на его функциях.
- Скорее всего на рынке уже есть подобные продукты и надо найти их, проведя исследования конкурентов.

## 4. Бизнес-требования и ограничения
#### 4.1 Функциональные требования

1. Пользователь может загрузить фотографию блюда.
2. Система анализирует фотографию и определяет количество калорий.
3. Результаты анализа отображаются пользователю.
4. (Для будущих версий) Возможность сохранять результаты для анализа и прогноза.
5. (Для будущих версий) Определение других пищевых характеристик.

#### 4.2 Нефункциональные требования

1. **Производительность**: быстрый ответ от системы после анализа изображения.
2. **Масштабируемость**: система должна поддерживать большое количество одновременных запросов.
3. **Безопасность**: защита данных пользователей и фотографий.

#### 4.3. Что входит в объем проекта/итерации, что не входит
MVP:
- В качестве интерфейса - TG-bot
- Без web-реализации
- Вся система монолитом, без разделения на сервисы
- Определение продуктов/блюд на фото
- Подсчёт калорий по стандартной порции из таблицы
- Без определения размера/веса
- Точность подсчёта калорий > 0.6 (Точность = 1 - MAPE)

В дальнейшем планируется определять размер/вес, увеличивать точность и разбивать систему на сервисы.

#### 4.4. Предпосылки решения
Планируется собрать данные и изображения из открытых источников и изучить подходы других команд к аналогичным задачам. Следует провести разведанализ данных в доменной области, чтобы выявить список наиболее распространённых блюд среди пользовательской аудитории, и в целом получить статистическую информацию.\
Если найденных изображений не будет достаточно, чтобы предоставить модели весь спектр классов для обучения, необходимо будет провести ручной сбор и разметку.\
В области обучения моделей следует опробовать разные архитектуры, в том числе State-Of-The-Art решения в сфере Computer Vision. Для этого обучить модели на едином датасете и далее сравнить результаты по выбранным метрикам. Также следует учитывать требовательность к ресурсам системы при инференсе модели.

Членам команды следует ознакомиться с доменной областью для понимания сути назначения сервиса.

Требования к текущей итерации проекта:
- Проект ведётся в приватном репозитории github
- Применяемая стратегия ветвления - GitHubFlow
- Документация размещается на GoogleDisk, либо (в дальнейшем) в репозитории с применением dvc-хранилища .
- Python код в виде .py скриптов и jupyter ноутбуков, соответствует стандартам PEP-8.
- DS-Стек: TensorFlow, PyTorch, стандартные data-science библиотеки.
- СУБД: PostgreSQL

#### 4.5. Ограничения
- Малый бюджет;
- Сложность разметки данных - желательно найти готовые датасеты;
- Отсутствие в команде профессиональных ML-Engineer, специализирующихся в данной области. Необходимо изучать применение SOTA решений самостоятельно;
- Отсутствие в команде экспертов в доменной области (Диетология). Необходимо найти связи с экспертами и максимально возможно изучить матчасть собственными силами.

## 5. Архитектура системы

### 5.1. Базовая архитектура.
На первом этапе планируется собрать монолитную архитектуру, в которой будут присутствовать:
- Telegram бот, являющийся интерфейсом пользователя и предоставляющий возможность загрузки фото. В ответ бот высылает сообщение с названиями продуктов/блюд, представленных на фото, и их калорийность.
- ML-модель, определяющая классы объектов, представленных на фото.
- Алгоритм, написанный на python, который по списку предоставленных ему продуктов (определённых ML-моделью) находит в СУБД стандартные порции и их калорийность. Далее алгоритм суммирует полученные результаты и передаёт их пользователю.
- СУБД на PostgreSQL, предоставляющая доступ к данным о размере порций и калорийности продуктов/блюд.


### 5.2 Развитая архитектура.
В дальнейшем планируется разбить систему на сервисы, выполняющие отдельные задачи.

#### Фронтенд
##### Web
- **Технология**: React
- **Основные функции**:
  1. Загрузка фотографий.
  2. Отображение результатов анализа.
  3. Ведение дневника
##### Telegram-bot
- **Технология**: Aiogram 3.x
- **Основные функции**:
  1. Загрузка фотографий
  2. Отображение результатов анализа.
  3. Ведение дневника
  4. Уведомления

#### Бэкенд
- **API**: FastAPI
- **База данных**: PostgreSQL
- **Модель ML** для распознавания изображения.
- **Алгоритм** расчета калорий по списку продуктов или блюд
- **ML-Алгоритм**, определяющий размеры объектов на фото

## 6. План разработки

1. Сбор и подготовка данных для обучения модели;
2. Разработка и обучение модели ML;
3. Разработка СУБД на PostgreSQL;
4. Разработка алгоритма подсчёта калорий;
5. Разработка TG-бота;
6. Создание API на FastAPI;
7. Разработка фронтенда на React;
8. Тестирование и оптимизация системы;
9. Добавление новых решений (в т.ч. определение размеров).
